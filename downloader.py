import os
import yaml
import subprocess

from sanitise import sanitise

from pprint import pprint

def download(path, entry):
    print('downloading {} to {}'.format(entry.id, path))
    os.makedirs(path, exist_ok=True)

    # some feeds have a 'link' field with extra information
    try:
        description_url = entry.link
        get_html(path, entry.title + '.html', description_url)
    except AttributeError:
        pass

    # download the main content
    for link in entry.links:
        print('fetching link type: ', link.type)
        if 'audio' in link.type:
            get_file(path, link.href)
        elif 'html' in link.type:
            get_html(path, entry.title + '.html', link.href)

    # save the metadata
    with open(os.path.join(path, 'entry.yaml'), 'w') as f:
        yaml.dump(entry, f, default_flow_style=False)

def get_html(path, name, url):
    wget_cmd = ['wget',
            '--adjust-extension', '--page-requisites', '--continue',
            '-O', sanitise(name),
            '-q', '-o', '/dev/null',
            url,
            ]
    p = subprocess.Popen(wget_cmd, cwd=path)
    p.wait()

def get_file(path, url):
    wget_cmd = ['wget', '--continue',
            '--content-disposition',
            url]
    p = subprocess.Popen(wget_cmd, cwd=path)
    p.wait()
