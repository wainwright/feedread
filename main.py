#!/usr/bin/python3

import os
import sys
import yaml
import argparse

import feed
import feedlist

config = None
the_list = None

def add_command(args, config):
    the_list.add(args.name, args.url)
    the_list.save()

def delete_command(args, config):
    try:
        the_list.delete(args.name)
    except KeyError:
        print('Entry not found: {}'.format(args.name))
    the_list.save()

def list_command(args, config):
    for k in sorted(the_list.data):
        print('{} ({})'.format(k, the_list.data[k]))

def update_command(args, config):
    for name, url in the_list.items():
        f = feed.Feed(config['path'], name, url, config['limit'])
        f.update()

def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='commands')
    
    add_subparse = subparsers.add_parser('add')
    add_subparse.set_defaults(func=add_command)
    add_subparse.add_argument('name')
    add_subparse.add_argument('url')

    delete_subparse = subparsers.add_parser('delete')
    delete_subparse.add_argument('name')
    delete_subparse.set_defaults(func=delete_command)

    list_subparse = subparsers.add_parser('list')
    list_subparse.set_defaults(func=list_command)

    update_subparse = subparsers.add_parser('update')
    update_subparse.set_defaults(func=update_command)

    return parser.parse_args(sys.argv[1:])

if __name__ == '__main__':
    with open(os.path.expanduser('~/.feedread.yaml'), 'r') as f:
        config = yaml.load(f, Loader=yaml.SafeLoader)
    the_list = feedlist.FeedList(config['path'])
    args = parse_args()
    args.func(args, config)
