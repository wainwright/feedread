import os
import yaml
import downloader
import feedparser

from sanitise import sanitise

class ItemList:

    def __init__(self, path):
        try:
            self.path = os.path.join(path, '.list')
            with open(self.path, 'r') as listfile:
                self.list = yaml.load(listfile, Loader=yaml.SafeLoader)
        except FileNotFoundError:
            self.list = set()

    def save(self):
        with open(self.path, 'w') as listfile:
            yaml.dump(self.list, listfile)

    def __contains__(self, obj):
        return obj in self.list

class Feed:

    def __init__(self, base_path, name, url, limit=3):
        self.base_path = os.path.expanduser(base_path)
        self.name = name
        self.url = url
        self.limit = limit

        self.path = os.path.join(self.base_path, name)

        os.makedirs(self.path, exist_ok=True)

        self.itemlist = ItemList(self.path)

        self.feed = feedparser.parse(url)

    def update(self):
        for entry in sorted(self.feed.entries[:self.limit],
                key=lambda x: x.published_parsed):
            print('updating ', entry.id, entry.published, '...')
            if entry.id in self.itemlist:
                print('have, skipping')
            else:
                self.download(entry)
                self.itemlist.list.add(entry.id)
                self.itemlist.save()
                print('done')

    def download(self, entry):
        name = sanitise(entry.title)
        print('path: ', self.path, name)
        path = os.path.join(self.path, name)
        downloader.download(path, entry)
