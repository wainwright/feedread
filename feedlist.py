import os
import yaml

class FeedList:
    
    def __init__(self, path):
        self.settings_path = os.path.expanduser(path)
        self.dbpath = os.path.join(self.settings_path, 'list.yaml')
        os.makedirs(self.settings_path, exist_ok=True)
        try:
            with open(self.dbpath, 'r') as dbfile:
                self.data = yaml.load(dbfile, Loader=yaml.SafeLoader)
        except FileNotFoundError:
            self.data = {}

    def save(self):
        with open(self.dbpath, 'w') as dbfile:
            yaml.dump(self.data, dbfile, default_flow_style=False)

    def add(self, name, url):
        self.data[name] = (url)

    def delete(self, name):
        del self.data[name]

    def items(self):
        return self.data.items()
