import string

valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
valid_chars = frozenset(valid_chars)
def sanitise(filename):
    filename = ''.join(c for c in filename if c in valid_chars)
    return filename[:250].strip()

